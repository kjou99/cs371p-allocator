// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <sstream> // istringstream
#include <string> // getline, string
#include <cassert> // assert
#include <vector>

using namespace std;

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;}                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);}

    public:
        // --------
        // typedefs
        // --------

        using      value_type = T;

        using       size_type = std::size_t;
        using difference_type = std::ptrdiff_t;

        using       pointer   =       value_type*;
        using const_pointer   = const value_type*;

        using       reference =       value_type&;
        using const_reference = const value_type&;

    public:
        // ---------------
        // iterator
        // over the blocks

        class iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const iterator& lhs, const iterator& rhs) {
                // <your code>
                if (lhs._p == rhs._p) return true;
                return false;}                                           // replace!

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const iterator& lhs, const iterator& rhs) {
                return !(lhs == rhs);}

            private:
                // ----
                // data
                // ----

                int* _p;
                int* _b; //beginning of memory
                int* _e; //ending of memory

            public:
                // -----------
                // constructor
                // -----------

                iterator (int* p) {
                    _p = p;
                    //_b = p;
                    //_e = (int*)((char*)p + N);
                }

                // ----------
                // operator *
                // ----------

                int& operator * () const {
                    // <your code>
                    return *_p;
                    /*static int tmp = 0;
                    return tmp;*/}           // replace!

                // -----------
                // operator ++
                // -----------

                iterator& operator ++ () {
                    // <your code>
                    int old = *_p;
                    if (old < 0) old*= -1;
                    char* tmp = (char*) _p;
                    tmp += old + 8;
                    _p = (int*) tmp;
                    //if (_p >= _e) {
                    //  _p = _e;
                    //}
                    return *this;}

                // -----------
                // operator ++
                // -----------

                iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}

                // -----------
                // operator --
                // -----------

                iterator& operator -- () {
                    // <your code>
                    /*int old = *_p;
                    if (old == *_e) {
                       old = 
                    }
                    if (old < 0) old*= -1;
                    char* tmp = (char*) _p;
                    tmp -= old + 8;
                    //if (old == *_e) tmp += 8;
                    _p = (int*) tmp;
                    if (_p <= _b) 
                        _p = _b;*/
                    char* tmp = (char*) _p;
                    //std::cout << "front " << _b << std::endl;
                    //std::cout << "N " << (int*) tmp << std::endl;
                    tmp -= 4;
                    _p = (int*) tmp;
                    //std::cout << "end sentinel " << (int*) tmp << std::endl;
                    int sz = (*_p > 0) ? *_p : *_p * -1;
                    //std::cout << sz << std::endl;
                    tmp -= (sz + 4);
                    //std::cout << "beginning sentinel " << (int*) tmp << std::endl;
                    _p = (int*) tmp;
                    //std::cout << _p << std::endl; 
                    /*if (_p <= _b) 
                        _p = _b;*/
                    return *this;}

                // -----------
                // operator --
                // -----------


                iterator operator -- (int) {
                    iterator x = *this;
                    --*this;
                    return x;}};

        // ---------------
        // const_iterator
        // over the blocks
        // ---------------

        class const_iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
                // <your code>
                if (lhs._p == rhs._p) return true;

                return false;}                                                       // replace!

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
                return !(lhs == rhs);}

            private:
                // ----
                // data
                // ----

                const int* _p;
                const int* _b; //beginning of memory
                const int* _e; //ending of memory


            public:
                // -----------
                // constructor
                // -----------

                const_iterator (const int* p) {
                    _p = p;
                    _b = p;
                    _e = (int*)((char*)p + N);

                }

                // ----------
                // operator *
                // ----------

                const int& operator * () const {
                    // <your code>
                    return *_p;
                    static int tmp = 0;
                    return tmp;}                 // replace!

                // -----------
                // operator ++
                // -----------

                const_iterator& operator ++ () {
                    // <your code>
                    int old = *_p;
                    if (old < 0) old*= -1;
                    char* tmp = (char*) _p;
                    tmp += old + 8;
                    _p = (int*) tmp;
                    if (_p >= _e) {
                        _p = _e;
                    }
                    return *this;}

                // -----------
                // operator ++
                // -----------

                const_iterator operator ++ (int) {
                    const_iterator x = *this;
                    ++*this;
                    return x;}

                // -----------
                // operator --
                // -----------

                const_iterator& operator -- () {
                    // <your code>
                    int old = *_p;
                    if (old < 0) old*= -1;
                    char* tmp = (char*) _p;
                    tmp += old + 8;
                    _p = (int*) tmp;
                    if (_p <= _b)
                        _p = _b;
                    return *this;}

                // -----------
                // operator --
                // -----------
                const_iterator operator -- (int) {
                    const_iterator x = *this;
                    --*this;
                    return x;}};

    private:
        // ----
        // data
        // ----

        char a[N];

        // -----
        // valid
        // -----

        /**
         * O(1) in space
         * O(n) in time
         * <your documentation>
         */
        bool valid () const {
            // <your code>
            // <use iterators>
            //using namespace std; //remove
            const_iterator b = begin();
            const_iterator e = end();
            int ind = -4;
            while (*b != *e) {
                int s1 = *b;
                ++b;
                ind += (s1 > 0) ? s1 + 8 : (s1 * -1) + 8;
                int s2 = *b;
                if (s2 != *e && s1 > 0 && s2 > 0) {
                    //cout << "two free blocks at " << ind << " values " << s1 << " and " << s2 << endl;
                    return false;
                }
                s2 = (*this)[ind];
                if (s2 != *e && s1 != s2) {
                    //cout << "sentinels don't match at " << ind << " values " << s1 << " and " << s2  << endl;
                    return false;
                }

            }
            return true;}

    public:
        // -----------
        // constructor
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
         */
        my_allocator () {
            auto min = sizeof(T) + (2* sizeof(int));
            if (N < min) throw std::bad_alloc();
            (*this)[0] = N-8; // replace!
            (*this)[N-4] = N-8;
            // <your code>
            assert(valid());}

        my_allocator             (const my_allocator&) = default;
        ~my_allocator            ()                    = default;
        my_allocator& operator = (const my_allocator&) = default;

        // --------
        // allocate
        // --------

        /**
         * O(1) in space
         * O(n) in time
         * after allocation there must be enough space left for a valid block
         * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
         * choose the first block that fits
         * throw a bad_alloc exception, if n is invalid
         */
        pointer allocate (size_type t) {
            // <your code>
            //using namespace std;
            iterator b = begin();
            iterator e = end();
            int s2 = -4;
            int s3;
            int s4;
            pointer p;
            while (*b != *e) {
                if (*b > (int)(t + 8)) {
                    int orig = *b;
                    int remaining = orig - (t + 8);
                    if (remaining > sizeof(T)) {
                        s4 = s2 + orig + 8;
                        s2 += t + 8;
                        s3 = s2 + 4;
                        t *= -1;
                        *b = t;
                        (*this)[s2] = t;
                        (*this)[s3] = remaining;
                        (*this)[s4] = remaining;
                    }
                    else {
                        s2 += orig + 8;
                        *b *= -1;
                        (*this)[s2] = *b;
                    }
                    //cout << s2 << endl;

                    //char* tmp = (char*) *b;
                    //tmp += (t + 4);
                    //b = (int*) tmp;
                    char* tmp = (char*) &*b;
                    tmp += 4;
                    p = (pointer) tmp;
                    break;
                }
                s2 += (*b > 0) ? *b + 8 : (*b * -1) + 8;
                ++b;
            }
            //cout << valid() << endl;
            //cout << (*this)[0] << " " << (*this)[44] << " " << (*this)[48] << " " << (*this)[76] << " " << (*this)[80] << " " << (*this)[996] << endl;
            assert(valid());
            return p;}             // replace!

        // ---------
        // construct
        // ---------

        /**
         * O(1) in space
         * O(1) in time
         */
        void construct (pointer p, const_reference v) {
            new (p) T(v);                               // this is correct and exempt
            assert(valid());}                           // from the prohibition of new

        // ----------
        // deallocate
        // ----------
        void deallocate (pointer p, size_type) {
            // <your code>
            char* ind = (char*) p;
            ind -= 4;
            int* s1 = (int*) ind;
            int* s2 = (int*) (ind + (*s1 * -1) + 4);
            *s1 *= -1;
            *s2 *= -1;
            //check left
            ind -= 4;
            int newBlock = *s1;
            int oldBlock = * (int*) ind;
            if (oldBlock > 0) {
                char* tmp = ind;
                tmp -= *ind;
                tmp -= 4;
                s1 = (int*) tmp;
                int newVal = *s1 + *s2 + 8;
                *s1 = newVal;
                *s2 = newVal;
            }
            //check right
            ind += newBlock + 12;
            oldBlock = * (int*) ind;
            if (oldBlock > 0) {
                char* tmp = ind;
                tmp += *ind + 4;
                s2 = (int*) tmp;
                int newVal = *s1 + *s2 + 8;
                *s1 = newVal;
                *s2 = newVal;

            }
            assert(valid());
            }

        // -------
        // destroy
        // -------

        /**
         * O(1) in space
         * O(1) in time
         */
        void destroy (pointer p) {
            p->~T();               // this is correct
            assert(valid());}

        // -----------
        // operator []
        // -----------
        int& operator [] (int i) {
            return *reinterpret_cast<int*>(&a[i]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const int& operator [] (int i) const {
            return *reinterpret_cast<const int*>(&a[i]);}

        // -----
        // begin
        // -----

        /**
         * O(1) in space
         * O(1) in time
         */
        iterator begin () {
            return iterator(&(*this)[0]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator begin () const {
            return const_iterator(&(*this)[0]);}

        // ---
        // end
        // ---

        /**
         * O(1) in space
         * O(1) in time
         */
        iterator end () {
            return iterator(&(*this)[N]);}

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator end () const {
            return const_iterator(&(*this)[N]);}};







void allocator_read(std::istream& sin, std::ostream& sout) {
    //sout << "new test" << endl;
    my_allocator<double, 1000> x;

    string s;
    vector<double*> blocks;
    while (getline(sin, s)) {
	//sout << "loop and read" << endl;
        if (s == "") 
	    break;
	if (s == "\n")
	    break;
	int block = stoi(s);
	if (block < 0) { //deallocate
	    block *= -1;
	    x.deallocate(blocks[block-1], sizeof(double));
	    blocks.erase(blocks.begin()+block-1);
	}
	else { //allocate
	    double* p = x.allocate(block * sizeof(double));
	    blocks.push_back(p);
	}
    }
    //output
    my_allocator<double, 1000>::iterator b = x.begin();
    my_allocator<double, 1000>::iterator e = x.end();
    while (*b != *e) {
        sout << *b << " ";
	++b;
    }
    /*cout << "ending " << *e << endl;
    --e;
    cout << "middle " << *e << endl;
    --e;
    cout << "beginning " << *e << endl;*/

}

// ----
// main
// ----

int main () {
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    the acceptance tests are hardwired to use my_allocator<double, 1000>
    */
    string s;
    //find number of test cases
    getline(cin, s);
    int numTests = stoi(s);
    assert(numTests > 0);
    assert(numTests <= 100);
    getline(cin, s);
    for (int i = 0; i < numTests; i++) {
	allocator_read(cin, cout);
        //if (i < (numTests - 1)) 
	    cout << endl;
    }
    //cout << "-40 944"     << endl;
    //cout << "-40 -24 912" << endl;
    //cout << "40 -24 912"  << endl;
    //cout << "72 -24 880"  << endl;
    return 0;}

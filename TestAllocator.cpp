// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

TEST(AllocatorFixture, test0) {
    using allocator_type = std::allocator<int>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TEST(AllocatorFixture, test1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// uncomment
TEST(AllocatorFixture, test2) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[0], 992);
}                                         // fix test

TEST(AllocatorFixture, test3) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[0], 992);
}                                         // fix test

// uncomment
TEST(AllocatorFixture, test4) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;                                            // read/write
    ASSERT_EQ(x[996], 992);
}                                         // fix test

TEST(AllocatorFixture, test5) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    const allocator_type x;                                      // read-only
    ASSERT_EQ(x[996], 992);
}                                         // fix test

TEST(AllocatorFixture, iter0) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    allocator_type::iterator b1 = x.begin();
    allocator_type::iterator b2 = x.begin();
    ASSERT_EQ(b1,b2);
}

TEST(AllocatorFixture, iter1) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    allocator_type y;
    allocator_type::iterator b1 = x.begin();
    allocator_type::iterator b2 = y.begin();
    bool eq = (b1 != b2);
    ASSERT_EQ(eq, true);
}

TEST(AllocatorFixture, iter2) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5);
    allocator_type::iterator b1 = x.begin();
    allocator_type::iterator b2 = x.begin();
    ++b2;
    bool eq = (b1 != b2);
    ASSERT_EQ(eq, true);
}

TEST(AllocatorFixture, iter3) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5);
    allocator_type::iterator b1 = x.begin();
    allocator_type::iterator b2 = x.begin();
    ++b2;
    --b2;
    ASSERT_EQ(*b1,*b2);
}

TEST(AllocatorFixture, iter4) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5);
    allocator_type::iterator b1 = x.begin();
    allocator_type::iterator b2 = x.begin();
    ++b2;
    ++b1;
    ASSERT_EQ(b1,b2);
}

TEST(AllocatorFixture, iter5) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    allocator_type::iterator b1 = x.begin();
    allocator_type::iterator b2 = x.end();
    ++b1;
    ASSERT_EQ(*b1,*b2);
}

TEST(AllocatorFixture, iter6) {
    using allocator_type = my_allocator<int, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    allocator_type::iterator b1 = x.begin();
    allocator_type::iterator b2 = x.end();
    --b2;
    ASSERT_EQ(*b1,*b2);
}

TEST(AllocatorFixture, alloc0) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5 * sizeof(double));
    allocator_type::iterator b = x.begin();
    ASSERT_EQ(*b, x[0]);
}

TEST(AllocatorFixture, alloc1) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5 * sizeof(double));
    allocator_type::iterator b = x.begin();
    ASSERT_EQ(*b, x[44]);
}

TEST(AllocatorFixture, alloc2) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5 * sizeof(double));
    x.allocate(3 * sizeof(double));
    allocator_type::iterator b = x.begin();
    ++b;
    --b;
    ASSERT_EQ(*b, x[0]);
}

TEST(AllocatorFixture, alloc3) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    x.allocate(5 * sizeof(double));
    x.allocate(3 * sizeof(double));
    x.allocate(5 * sizeof(double));
    allocator_type::iterator b = x.begin();
    allocator_type::iterator e = x.end();
    ++b;
    ++b;
    ++b;
    ++b;
    ASSERT_EQ(*b, *e);
}

TEST(AllocatorFixture, dealloc0) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    double* a = x.allocate(5 * sizeof(double));
    double* b = x.allocate(3 * sizeof(double));
    double* c = x.allocate(3 * sizeof(double));
    double* d = x.allocate(3 * sizeof(double));
    x.deallocate(a, sizeof(double));
    x.deallocate(c, sizeof(double));
    x.deallocate(b, sizeof(double));
    ASSERT_EQ(x[0], 104);

}

TEST(AllocatorFixture, dealloc1) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    double* a = x.allocate(5 * sizeof(double));
    double* b = x.allocate(3 * sizeof(double));
    double* c = x.allocate(3 * sizeof(double));
    double* d = x.allocate(3 * sizeof(double));
    x.deallocate(a, sizeof(double));
    x.deallocate(c, sizeof(double));
    x.deallocate(b, sizeof(double));
    ASSERT_EQ(x[108], 104);

}

TEST(AllocatorFixture, sizecheck0) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    double* a = x.allocate(123 * sizeof(double));
    ASSERT_EQ(x[0], -992);

}

TEST(AllocatorFixture, sizecheck1) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    double* a = x.allocate(123 * sizeof(double));
    ASSERT_EQ(x[996], -992);

}

TEST(AllocatorFixture, sizecheck2) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    double* a = x.allocate(124 * sizeof(double));
    ASSERT_EQ(x[0], -992);

}

TEST(AllocatorFixture, sizecheck3) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    double* a = x.allocate(122 * sizeof(double));
    double* b = x.allocate(1 * sizeof(double));
    ASSERT_EQ(x[0], -976);
}

TEST(AllocatorFixture, sizecheck4) {
    using allocator_type = my_allocator<double, 1000>;
    using value_type     = typename allocator_type::value_type;
    using size_type      = typename allocator_type::size_type;
    using pointer        = typename allocator_type::pointer;

    allocator_type x;
    double* a = x.allocate(122 * sizeof(double));
    double* b = x.allocate(1 * sizeof(double));
    ASSERT_EQ(x[984], -8);
}


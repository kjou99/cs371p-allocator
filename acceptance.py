import random

def writetests():
    with open("kjou99-RunAllocator.in", "w") as file:
        numTests = 35
        file.write(str(numTests))
        # 20 lines per test case
        for i in range(numTests):
            print str(i)
            size = 1000
            file.write("\n")
            numBlocks = 0
            lines = 0
            while ((size - 8) / 8) > 0 and lines < 30:
                #print str(lines)
                lines+= 1
                block = 0
                while block == 0:
                    block = random.randint(numBlocks * -1,((size - 8) / 8))
                file.write(str(block) + "\n")
                if block < 0:
                    size += ((block * 8) + 8)
                    numBlocks-=1
                else:
                    size -= ((block * 8) + 8)
                    numBlocks+=1


if __name__ == "__main__":
    writetests()

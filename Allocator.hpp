// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            if (lhs._p == rhs._p) return true;
            return false;
        }                                           // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;
        int* _b; //beginning of memory
        int* _e; //ending of memory

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
            _e = (int*)((char*)p + N);
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            int old = *_p;
            if (old < 0) old*= -1;
            char* tmp = (char*) _p;
            tmp += old + 8;
            _p = (int*) tmp;
            if (_p >= _e) { //point to end
                _p = _e;
            }
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            char* tmp = (char*) _p;
            tmp -= 4;
            _p = (int*) tmp;
            int sz = (*_p > 0) ? *_p : *_p * -1;
            tmp -= (sz + 4);
            _p = (int*) tmp;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            if (lhs._p == rhs._p) return true;
            return false;
        }                                                       // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;
        const int* _b; //beginning of memory
        const int* _e; //ending of memory


    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
            _e = (int*)((char*)p + N);

        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            // <your code>
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            int old = *_p;
            if (old < 0) old*= -1;
            char* tmp = (char*) _p;
            tmp += old + 8;
            _p = (int*) tmp;
            if (_p >= _e) {
                _p = _e;
            }
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            char* tmp = (char*) _p;
            tmp -= 4;
            _p = (int*) tmp;
            int sz = (*_p > 0) ? *_p : *_p * -1;
            tmp -= (sz + 4);
            _p = (int*) tmp;

            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    bool valid () const {
        // <your code>
        // <use iterators>
        const_iterator b = begin();
        const_iterator e = end();
        int index = 0;

        while (*b != *e || &*b < &*e) {
            int s1 = *b;
            ++b;
            int s3 = *b;
            int s2;
            if (s3 != *e && s1 > 0 && s3 > 0) {
                //cout << "two free blocks at " << index << " and next" << endl;
                return false;
            }
            index += (s1 > 0) ? s1 + 4 : (s1 * -1) + 4;
            if (index < 0 || index >= N) return true;
            s2 = (*this)[index];
            index += 4;
            if (s2 != *e && s1 != s2) {
                //cout << "sentinels don't match at " << index << " values " << s1 << " and " << s2  << endl;
                return false;
            }
            if (s2 == *e) return true;

        }

        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        auto min = sizeof(T) + (2* sizeof(int));
        //std::cout << "min " << min << std::endl;
        if (N < min)
            throw std::bad_alloc();
        (*this)[0] = N-8; // replace!
        (*this)[N-4] = N-8;
        // <your code>
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type t) {
        // <your code>
        iterator b = begin();
        iterator e = end();
        int s2 = -4;
        int s3;
        int s4;
        pointer p;
        bool allocated = false;
        //std::cout << "size is " << t + 8 << std::endl;
        while (*b != *e) {
            //std::cout << "block size " << *b << std::endl;
            if (*b >= (int)t) {
                allocated = true;
                int orig = *b;
                int remaining = orig - (t + 8);
                if (remaining >= sizeof(T)) {
                    s4 = s2 + orig + 8;
                    s2 += t + 8;
                    s3 = s2 + 4;
                    t *= -1;
                    *b = t;
                    (*this)[s2] = t;
                    (*this)[s3] = remaining;
                    (*this)[s4] = remaining;
                }
                else {
                    s2 += orig + 8;
                    *b *= -1;
                    (*this)[s2] = *b;
                }
                char* tmp = (char*) &*b;
                tmp += 4;
                p = (pointer) tmp;
                allocated = true;
                break;
            }
            s2 += (*b > 0) ? *b + 8 : (*b * -1) + 8;
            ++b;
        }
        //std::cout << "allocated? " << allocated << std::endl;
        if (!allocated)
            throw std::bad_alloc();
        assert(valid());
        return p;
    }             // replace!

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type) {
        // <your code>
        char* ind = (char*) p;
        ind -= 4;
        int* s1 = (int*) ind;
        int* s2 = (int*) (ind + (*s1 * -1) + 4);
        *s1 *= -1;
        *s2 *= -1;
        //coalescing
        iterator b = begin();
        iterator e = end();
        //cout << "edges " << &*b << " " << &*e << endl;
        //cout << "sentinels " << s1 << " " << s2 << " size " << " " << *s1 << " " << *s2 << endl;
        bool left = false;
        bool right = false;
        if (&*b == s1) left = true;
        if (&*e == (s2 + 1)) right = true;
        if (!left) { //see if left is outside array
            int* s4 = (int*) (ind - 4);
            //cout << "not far left " << endl;
            //cout << "right sentinel " << s4 << endl;
            if (*s4 > 0) {
                //cout << "adding on left" << endl;
                int sz = *s4 + *s1;
                ind -= (*s4 + 8);
                int* s3 = (int*) ind;
                //cout << "left sentinel " << s3 << endl;
                //cout << "right sentinel " << s4 << endl;
                *s3 = sz + 8;
                *s2 = sz + 8;
                s1 = s3;
            }
        }
        if (!right) { //see if right is outside array
            //cout << "not far right " << endl;
            ind += (*s1 + 8);
            int* s3 = (int*) ind;
            //cout << "left sentinel " << s3 << endl;
            if (*s3 > 0) {
                //cout << "adding on right" << endl;
                int sz = *s3 + *s1;
                int* s4 = (int*) (ind + *s3 + 4);
                //cout << "right sentinel " << s4 << endl;
                *s1 = sz + 8;
                *s4 = sz + 8;

            }
        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }
};

#endif // Allocator_h
